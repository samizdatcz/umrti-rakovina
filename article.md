---
title: "Data: V úmrtí na rakovinu tlustého střeva už nejsme evropská jednička"
perex: "Diagnóza typická pro střední Evropu straší Česko od nepaměti. Přitom je patrné zlepšení, umírá na ni o 40 procent méně Čechů než před dvaceti lety"
description: "Diagnóza typická pro střední Evropu straší Česko od nepaměti. Přitom je patrné zlepšení, umírá na ni o 40 procent méně Čechů než před dvaceti lety"
authors: ["Jan Boček", "Jan Cibulka"]
published: "27. října 2016"
coverimg: https://interaktivni.rozhlas.cz/umrti-rakovina/media/cover.jpg
socialimg: https://interaktivni.rozhlas.cz/umrti-rakovina/media/socimg2.png
coverimg_note: "Foto <a href='https://www.flickr.com/photos/govbeshear/6959790317/'>Steve Bashear: Colon Cancer Awareness Rally</a> (CC BY-NC-ND 2.0)"
url: "umrti-rakovina"
libraries: [topojson, leaflet, jquery]
recommended:
  - link: https://interaktivni.rozhlas.cz/umrti-srdce/
    title: Na co se kde v Evropě nejčastěji umírá? Podívejte se na podrobnou mapu
    perex: Češi mají díky lékařské péči největší šanci na přežití srdečního infarktu na světě. Ani to ale nestačí: počtem úmrtí na nemoci srdce patříme do méně civilizované části Evropy. Umíráme na ně třikrát častěji než Francouzi.
    image: https://interaktivni.rozhlas.cz/umrti-srdce/media/socimg.png
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/rakovina-nemusi-byt-desiva-podivejte-se-jaka-je-prognoza-u-konkretnich-diagnoz--1527501
    title: Rakovina nemusí být děsivá. Podívejte se, jaká je prognóza u konkrétních diagnóz
    perex: Věta „máte rakovinu“ nemusí být tak tragická, jak jsme zvyklí ji vnímat. Pokud je nemoc odhalena včas, šance na přežití může být téměř stoprocentní.
    image: http://media.rozhlas.cz/_obrazek/2034747--onkologicke-pracoviste--1-800x534p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/rusove-proti-rusum-zeme-v-nevyhlasene-obcanske-valce--1484099
    title: Rusové proti Rusům: Země v nevyhlášené občanské válce
    perex: Rusko prošlo v devadesátých letech demografickou krizí srovnatelnou s válečným konfliktem. Za Putinovy vlády se pád do propasti zpomalil, snad i zastavil. To ale nemusí vydržet dlouho: nynější růst stojí na velmi křehkých základech.
    image: http://media.rozhlas.cz/_obrazek/3372356.jpeg
---

<aside class="small">
  <iframe src="https://samizdat.cz/data/mapa-eumrti/charts/strevo.html" class="ig" width="100%" height="400px" scrolling="no" frameborder="0"></iframe>
</aside>

Ve výskytu rakoviny tlustého střeva patří Češi už mnoho let mezi světovou „špičku“. Situace se ale zlepšuje.

„V úmrtnosti bohužel stále patříme mezi nejpostiženější evropské státy, ale dochází k velmi nadějnému zlepšení,“ vysvětluje Ondřej Májek z Institutu biostatistiky a analýz brněnské Masarykovy univerzity. „Za rychlejší snižování mortality zřejmě vděčíme úspěšnější léčbě a také včasnějšímu záchytu díky organizovanému screeningovému programu.“

Data Eurostatu jeho slova potvrzují, úmrtnost na rakovinu tlustého střeva v Česku od poloviny devadesátých let klesá. Dnes na ni umírá o 40 procent méně Čechů než v roce 1994.

Pozitivní trend potvrzuje i [aktuální zpráva OECD o zdravotnictví](http://www.keepeek.com/Digital-Asset-Management/oecd/social-issues-migration-health/health-at-a-glance-2015_health_glance-2015-en#.WBCkqfmLSM8). Česko chválí za zlepšení účinnosti léčby. Jedním dechem ovšem připomíná, že přes pokrok v léčení stále patříme mezi vyspělými zeměmi OECD ve statistice přežívání k méně úspěšným.

Rakovina tlustého střeva je v české populaci druhým nejrozšířenějším zhoubným nádorem. Data národního onkologického registru ukazují, že obvykle začínají hrozit po padesátém roce života. Na tento věk se proto lékaři snaží zaměřit screeningový program.

<aside class="big">
  <iframe src="https://samizdat.cz/data/rakovina-doziti-www/www/#veky" class="ig" width="100%" height="2000px" scrolling="no" frameborder="0"></iframe>
</aside>

Díky zlepšení si Česko nevede úplně špatně ani v mezinárodním srovnání: podobnou úmrtnost jako Česko má třeba Dánsko, většina Nizozemska nebo jižní část Norska včetně hlavního města. Podstatně vyšší je naopak úmrtnost v Chorvatsku nebo na západním Slovensku, vůbec nejhůř je na tom Maďarsko.

<aside class="big">
  <div class="selector" id="selector" onchange="checkSelect()">
    <select id="diagnoza" name="diagnoza">
      <option value="diagnoza9">Rakovina tlustého střeva</option>
      <option value="diagnoza8">Rakovina prsu</option>
      <option value="diagnoza10">Rakovina prostaty</option>
      <option value="diagnoza11">Rakovina plic</option>
      <option value="diagnoza1">Celková úmrtnost</option>
      <option value="diagnoza3">Nemoci oběhové soustavy</option>
      <option value="diagnoza4">Nemoci dýchací soustavy</option>
      <option value="diagnoza14">Cukrovka</option>
      <option value="diagnoza6">HIV</option>
      <option value="diagnoza5">Vnější příčiny</option>
      <option value="diagnoza7">Sebevražda</option>
      <option value="diagnoza12">Dopravní nehody</option>
      <option value="diagnoza2">Napadení</option>
      <option value="diagnoza13">Otrava (včetně alkoholu)</option>
      <option value="diagnoza15">Pády</option>
    </select>
  </div>
  <div id="mapid"></div>
</aside>

*Mapa ukazuje takzvanou standardizovanou mortalitu: roční počet úmrtí na 100 000 obyvatel daného území, přepočtený na stejnou věkovou strukturu. Tím se eliminuje vliv různého průměrného věku na jednotlivých územích, a čísla lze proto navzájem srovnávat. Mortalita přímo souvisí s očekávanou délkou dožití – tam, kde je nižší, se žije déle. Čísla jsou průměr za roky 2011 až 2013.*

## Na západě Evropy se častěji dožívají rakoviny

Na rozdíl od nemocí oběhové soustavy, kde je zřejmá [propast mezi západem a východem](https://interaktivni.rozhlas.cz/umrti-srdce/), nemá úmrtnost na novotvary na ose západ-východ jednoznačné prostorové rozložení. Část diagnóz se soustředí do úzce vymezených oblastí, jiné typy rakoviny jsou častější v celé západní Evropě. Může za to fakt, že se v těchto zemích dožívají vyššího věku a ten je zase spojen s vyšším výskytem onkologických onemocnění.

„Každý muž starší osmdesáti let má nějaké stadium rakoviny prostaty,“ vysvětluje profesor Jan Marek z Institutu kardiovaskulárních chorob na londýnské University College. „Obvykle na ni ale neumře, protože buňky se ve stáří dělí pomaleji.“

Na mapě úmrtnosti lze sledovat oblasti, kde se prosazuje některý typ rakoviny. Je to například Skandinávie, pro kterou je – zřejmě právě kvůli vysokému věku dožití – typická rakovina prostaty. Rakovina prsu je typická pro západ Německa a Benelux, stejně jako pro oblasti kolem Bratislavy a Budapešti.

Výrazně lokální je úmrtnost na rakovinu plic a s ní spojené nemoci dýchací soustavy. „Když srovnáte mapu respiračních onemocnění s mapou lokalit těžkého strojírenství a dolů, budou se krásně překrývat,“ komentuje jejich výskyt profesor Marek.

<aside class="small">
  <iframe src="https://samizdat.cz/data/mapa-eumrti/charts/plica.html" class="ig" width="100%" height="400px" scrolling="no" frameborder="0"></iframe>
</aside>

Úmrtnost na rakovinu plic zároveň vykazuje protichůdný trend u mužů a žen: zatímco u mužů za posledních dvacet let poklesla standardizovaná míra úmrtnosti o 36 procent, u žen se ve stejném období o 26 procent zvýšila.

„Bylo by přínosné rovněž zkoumat prostorové rozložení rakoviny slinivky břišní,“ doplňuje mapu úmrtnosti Ondřej Májek z Institutu biostatistiky a analýz. „Jedná se o velmi problematický zhoubný nádor s rostoucím počtem výskytů a velmi nízkou úspěšností léčby. „Zdá se, že nádor slinivky břišní je typický pro střední a severovýchodní Evropu.“

## U některých diagnóz je šance přežít téměř stoprocentní

Úmrtnost na onkologická onemocnění může mít různé příčiny, nejpodstatnějším faktorem pro úspěšnost léčby rakoviny je ovšem včasná diagnóza.

„Obecně v onkologii platí, že čím dříve na rakovinu přijdeme, tím větší je šance na úplné uzdravení,“ potvrzuje Rostislav Vyzula z Masarykova onkologického ústavu. „Co znamená včas, je však pro různá onemocnění různé. U rakoviny prsu je včasná diagnóza podstatná, u rakoviny prostaty méně. Je to dáno agresivitou onemocnění.“

Vliv screeningu – a tedy včasné diagnózy – ilustruje následující graf. Ukazuje relativní šanci na pětileté přežití rakoviny na základě stadia, kdy je odhalena. *Relativní* znamená, že jde o srovnání se zdravou populací ve stejném věku.

Podstatné je u šance na přežití to, u jakého procenta pacientů s danou diagnózou se vůbec k léčení přistupuje: to ukazuje červený sloupec u každé diagnózy. Graf tedy například říká, že úspěšnost pětiletého přežití u rakoviny tlustého střeva je obecně 54 procent, výrazně se ovšem liší u jednotlivých stadií. Pokud je nemoc zachycena v prvním stadiu, léčí se téměř 90 procent nemocných a šance na přežití je 86 procent. V posledním čtvrtém stadiu se léčí dvě třetiny nemocných a šance na přežití klesá na pouhých 10 procent.

<aside class="big">
  <iframe src="https://samizdat.cz/data/rakovina-doziti-www/www/#lecenost" class="ig" width="100%" height="700px" scrolling="no" frameborder="0"></iframe>
</aside>